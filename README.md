## shiny-server

This repo contains code used on my personal Shiny Server instance.

Some of these apps can be found here: https://heds.nz/projects/

I've documented the process to set up an instance of Shiny Server on a Ubuntu 20.04 Linux box [here](https://heds.nz/posts/ubuntu-shiny-server/).