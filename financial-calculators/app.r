# This should source automatically, I don't know why I have to say this.
source("r/global.R")

ui <- tagList(
    includeCSS('style.css'),
    navbarPage(
        theme = bs_theme(bootswatch = "minty"),
        title = "heds.nz's financial calculators",
        tabPanel("Rental value", rental_value_ui("rental_value")),
        tabPanel("FI/RE projections", projs_ui("projs")),
        tabPanel("Usable equity", usable_equity_ui("usable_equity")),
        tabPanel("Cashflow", cashflow_ui("cashflow")),
        tabPanel("About", about_ui("about"))
    )
)

server <- function(input, output, session) {
    about_server("about")
    projs_server("projs")
    rental_value_server("rental_value")
    usable_equity_server("usable_equity")
    cashflow_server("cashflow")
}

shinyApp(ui, server)
